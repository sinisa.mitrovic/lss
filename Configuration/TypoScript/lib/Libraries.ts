lib.logo = TEXT
lib.logo {
    value = <img src="typo3conf/ext/tool_provider/Resources/Public/images/logo.png" alt="LSS" />
    typolink.parameter = 1
}

lib.pageTitle = TEXT
lib.pageTitle {
    data = page : title
    wrap = <h2> | </h2>
}

lib.contentLeft = CONTENT
lib.contentLeft {
    table = tt_content
    select.where = colpos=0
}

lib.contentRight = CONTENT
lib.contentRight {
    table = tt_content
    select.where = colpos=1
}


lib.footerFirst = CONTENT
lib.footerFirst {
    table = tt_content
    select {
        pidInList = {$lib.footerPageUid}
        where = colPos = 0
        languageField = sys_language_uid
        orderBy = sorting
    }
}
lib.footerSecond = CONTENT
lib.footerSecond {
    table = tt_content
    select {
        pidInList = {$lib.footerPageUid}
        where = colPos = 1
        languageField = sys_language_uid
        orderBy = sorting
    }
}

lib.footerThird = CONTENT
lib.footerThird {
    table = tt_content
    select {
        pidInList = {$lib.footerPageUid}
        where = colPos = 2
        languageField = sys_language_uid
        orderBy = sorting
    }
}

lib.footerFourth = CONTENT
lib.footerFourth {
    table = tt_content
    select {
        pidInList = {$lib.footerPageUid}
        where = colPos = 3
        languageField = sys_language_uid
        orderBy = sorting
    }
}

lib.copyright = COA
lib.copyright {
    10 = TEXT
    10 {
        data = date:U
        strftime =%Y
        wrap = <div class="eight columns"><div id="copyright">&copy;&nbsp; | &nbsp;Copyright by Language Solutions Studio. All Rights Reserved.</div></div>
    }

    20 = COA
    20 {
        wrap = <div class="eight columns"><ul class="social-icons-footer"> | </ul></div>
        10 = TEXT
        10.value = <li><a href="https://twitter.com/LangueSolutions" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>

        20 = TEXT
        20.value = <li><a href="https://www.facebook.com/LanguageSolutionsStudio" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
    }
}



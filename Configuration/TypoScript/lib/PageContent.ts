lib.pageContent = FLUIDTEMPLATE
lib.pageContent {


    templateName = TEXT
    templateName.stdWrap {
        cObject = TEXT
        cObject {
            data = pagelayout
            split {
                token = pagets__
                1.current = 1
                1.wrap = |
            }
        }
    }


    templateRootPaths {
        10 = EXT:tool_provider/Resources/Private/Templates
        20 = {$plugin.tx_toolprovider.view.templateRootPaths}
    }
    layoutRootPaths {
        10 = EXT:tool_provider/Resources/Private/Layouts
        20 = {$plugin.tx_toolprovider.view.layoutRootPaths}
    }
    partialRootPaths {
        10 = EXT:tool_provider/Resources/Private/Partials
        20 = {$plugin.tx_toolprovider.view.partialRootPaths}
    }

    variables {

        contentLeft < lib.contentLeft
        contentRight < lib.contentRight
        logo < lib.logo
        language < lib.language
        mainMenu < lib.mainMenu
        pageTitle < lib.pageTitle
        breadcrumb < lib.breadcrumb
        footerFirst < lib.footerFirst
        footerSecond < lib.footerSecond
        footerThird < lib.footerThird
        footerFourth < lib.footerFourth
        copyright < lib.copyright

    }
    extbase.controllerExtensionName = ToolProvider
}


# extend lib.contentElement from fluid_styled_content
lib.contentElement {
    templateRootPaths {
        20 = EXT:tool_provider/Resources/Private/Templates/ContentElements/
    }
    partialRootPaths {
        20 = EXT:tool_provider/Resources/Private/Partials/ContentElements/
    }
    layoutRootPaths {
        20 = EXT:tool_provider/Resources/Private/Layouts/ContentElements/
    }
}
plugin.tx_toolprovider {
    view {
        templateRootPaths.0 = {$plugin.tx_toolprovider.view.templateRootPaths.0}
        partialRootPaths.0 = {$plugin.tx_toolprovider.view.partialRootPaths.0}
        layoutRootPaths.0 = {$plugin.tx_toolprovider.view.layoutRootPaths.0}
    }
}
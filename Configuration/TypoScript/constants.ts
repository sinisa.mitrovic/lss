plugin.tx_toolprovider.view {
	templateRootPaths = EXT:tool_provider/Resources/Private/Templates/
	partialRootPaths = EXT:tool_provider/Resources/Private/Partials/
	layoutRootPaths = EXT:tool_provider/Resources/Private/Layouts/
}

styles.content.imgtext.maxW = 1920
styles.content.imgtext.maxWInText = 1920
styles.content.imgtext.linkWrap.width = 800


styles {
    content {
        textmedia {
        # maximum width of generated images
            maxW = 1920
            # maximum width of generated images (beside text)
            maxWInText = 1920
            # column spacing in pixels for multiple image columns
            columnSpacing = 10
            # row spacing in pixels for multiple image rows
            rowSpacing = 10
            # spacing to the text
            textMargin = 10
        }
        textpic {
        # maximum width of generated images
            maxW = 1920
            # maximum width of generated images (beside text)
            maxWInText = 1920
            # column spacing in pixels for multiple image columns
            columnSpacing = 10
            # row spacing in pixels for multiple image rows
            rowSpacing = 10
            # spacing to the text
            textMargin = 10
        }
    }
}

# cat=plugin.tx_toolprovider/page; type=integer; label=Footer page uid (BE)
lib.footerPageUid = 9
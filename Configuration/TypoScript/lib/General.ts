config {
    prefixLocalAnchors = 0
    baseURL = http://www.lss.com/

    linkVars = L
    uniqueLinkVars = 1
    sys_language_overlay = hideNonTranslated
    sys_language_mode = content_fallback
    #sys_language_softMergeIfNotBlank = tt_content:image

    sys_language_uid = 0
    language = rs
    locale_all = rs_RS


    tx_realurl_enable = 1
    spamProtectEmailAddresses = ascii
    spamProtectEmailAddresses_atSubst = &#64;
    fileTarget = _blank
    meaningfulTempFilePrefix = 100

    # Kommentare aus dem Quelltext entfernen
    disablePrefixComment = 1

    pageTitleFirst = 1

    // Compression and Concatenation of CSS and JS Files
    compressJs                          = 1
    compressCss                         = 1
    concatenateJs                       = 1
    concatenateCss                      = 1
}

[globalString = ENV:HTTP_HOST=language-solutions-studio.psmitrovic.com]
config.baseURL = http://language-solutions-studio.psmitrovic.com/
[global]

# Englishlanguage, sys_language.uid = 1
[globalVar = GP:L = 1]
config.sys_language_uid = 1
config.language = en
config.locale_all = english.UTF8
config.htmlTag_langKey = en
config.htmlTag_setParams = lang="en"
plugin.tx_indexedsearch._DEFAULT_PI_VARS.lang = 1
[GLOBAL]


config.htmlTag_stdWrap = COA
config.htmlTag_stdWrap {
    # additional html classes
    10 = COA
    10 {
        wrap = class="|"
        10 = TEXT
        10.data = TSFE:sys_language_isocode
    }
}

# IMAGE WIDTH FIX 
tt_content.image.20.maxW = 1920
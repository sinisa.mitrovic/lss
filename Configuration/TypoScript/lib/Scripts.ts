page {
    includeCSS {
        style = EXT:tool_provider/Resources/Public/css/style.css
    }    

    includeJSFooterlibs {
        jquery = http://code.jquery.com/jquery-1.11.0.min.js
        jquery.external = 1
    }

    includeJSFooter {
        jqueryMigrate = http://code.jquery.com/jquery-migrate-1.2.1.min.js
        jqueryMigrate.external = 1
        flexslider = EXT:tool_provider/Resources/Public/js/flexslider.js
        isotope = EXT:tool_provider/Resources/Public/js/jquery.isotope.min.js
        custom = EXT:tool_provider/Resources/Public/js/custom.js
        enderMin = EXT:tool_provider/Resources/Public/js/ender.min.js
        selectnav = EXT:tool_provider/Resources/Public/js/selectnav.js
        imageboxMin = EXT:tool_provider/Resources/Public/js/imagebox.min.js
        carousel = EXT:tool_provider/Resources/Public/js/carousel.js
        twitter = EXT:tool_provider/Resources/Public/js/twitter.js
        tooltip = EXT:tool_provider/Resources/Public/js/tooltip.js
        popover = EXT:tool_provider/Resources/Public/js/popover.js
        layersliderTransitions = EXT:tool_provider/Resources/Public/js/layerslider.transitions.js
        layersliderKreaturamediaJquery = EXT:tool_provider/Resources/Public/js/layerslider.kreaturamedia.jquery.js
        greensock = EXT:tool_provider/Resources/Public/js/greensock.js
        imageboxBuild = EXT:tool_provider/Resources/Public/js/imagebox.build.js

    }
}

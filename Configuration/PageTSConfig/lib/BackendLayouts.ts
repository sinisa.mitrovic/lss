mod {
    web_layout {
        BackendLayouts {
            Home {
                title = Home
                config {
                    backend_layout {
                        colCount = 2
                        rowCount = 6
                        rows {
                            1 {
                                columns {
                                    1 {
                                        name = Intro left
                                        colPos = 0
                                        colspan = 6
                                    }

                                }
                            }
                            2 {
                                columns {
                                    1 {
                                        name = Teaser
                                        colPos = 2
                                        colspan = 12
                                    }
                                }
                            }
                            3 {
                                columns {
                                    1 {
                                        name = News
                                        colPos = 3
                                        colspan = 8
                                    }
                                    2 {
                                        name = Events
                                        colPos = 4
                                        colspan = 4
                                    }
                                }
                            }
                            4 {
                                columns {
                                    1 {
                                        name = Team
                                        colPos = 5
                                        colspan = 12
                                    }
                                }
                            }
                            5 {
                                columns {
                                    1 {
                                        name = Testimonial left
                                        colPos = 6
                                        colspan = 6
                                    }
                                    2 {
                                        name = Testimonial right
                                        colPos = 7
                                        colspan = 6
                                    }
                                }
                            }
                            6 {
                                columns {
                                    1 {
                                        name = Gallery
                                        colPos = 8
                                        colspan = 12
                                    }
                                }
                            }
                        }
                    }
                }
            }
            ContentPage {
                title = Content Page
                config {
                    backend_layout {
                        colCount = 2
                        rowCount = 1
                        rows {
                            1 {
                                columns {
                                    1 {
                                        name = Content left
                                        colPos = 0
                                        colspan = 9
                                    }
                                    2 {
                                        name = Content right
                                        colPos = 1
                                        colspan = 3
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Footer {
                title = Footer
                config {
                    backend_layout {
                        colCount = 4
                        rowCount = 1
                        rows {
                            1 {
                                columns {
                                    1 {
                                        name = Footer first
                                        colPos = 0
                                        colspan = 4
                                    }
                                    2 {
                                        name = Footer second
                                        colPos = 1
                                        colspan = 4
                                    }
                                    3 {
                                        name = Footer third
                                        colPos = 2
                                        colspan = 4
                                    }
                                    4 {
                                        name = Footer fourth
                                        colPos = 3
                                        colspan = 4
                                    }
                                }
                            }
                        }
                    }
                }
            }
            SubsiteWithHeader  {
                title = Subsite with Header
                    config {
                    backend_layout {
                        colCount = 1
                        rowCount = 1
                        rows {
                            1 {
                                columns {
                                    1 {
                                        name = Content
                                        colPos = 0
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
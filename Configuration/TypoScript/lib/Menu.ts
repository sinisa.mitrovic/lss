# ======================================================================
# Menu configuration with additional classes for first/last menu elements
# ======================================================================

lib.basemenu = HMENU
lib.basemenu {
    1 = TMENU
    1 {
        wrap = <ul>|</ul>

        NO = 1
        NO {
            allStdWrap.insertData = 1
            # ATagTitle.field = description // subtitle // title

            stdWrap.cObject = COA
            stdWrap.cObject {
                10 = TEXT
                10.field = nav_title // title // subtitle 

                12 = LOAD_REGISTER
                12 {
                    level1title.cObject = RECORDS
                    level1title.cObject {
                        source = {field:pid}
                        source.insertData = 1
                        tables = pages
                        conf.pages = TEXT
                        conf.pages.field = title
                    }
                }

                # Get the unix timestamp from newUntil w/o time and add 23:59:59
                15 = LOAD_REGISTER
                15 {
                    today {
                        cObject = TEXT
                        cObject {
                            data = field:newUntil
                            wrap = |+86399
                        }

                        prioriCalc = intval
                    }
                }

                20 = LOAD_REGISTER
                20 {
                    new.cObject = TEXT
                    new.cObject {
                        value = new
                        if {
                            value.data = date: U
                            isLessThan.data = register:today
                            negate = 1
                        }
                    }
                }

                30 = LOAD_REGISTER
                30 {
                    firstnew < lib.basemenu.1.NO.stdWrap.cObject.20.new
                    firstnew.cObject.value = firstnew
                }

                40 = LOAD_REGISTER
                40 {
                    lastnew < lib.basemenu.1.NO.stdWrap.cObject.20.new
                    lastnew.cObject.value = new
                }
            }

            wrapItemAndSub.insertData = 1
            wrapItemAndSub = <li class="first {register:new} {register:firstnew} page-{field:uid}">|</li> |*| <li class="{register:new} page-{field:uid}">|</li> |*| <li class="last {register:new} {register:lastnew} page-{field:uid}">|</li>
            linkWrap = |
        }

        ACT < .NO
        ACT = 1
        ACT {
            wrapItemAndSub = <li class="active first {register:new} {register:firstnew} page-{field:uid}">|</li> |*| <li class="active {register:new} page-{field:uid}">|</li> |*| <li class="active last {register:new} {register:lastnew} page-{field:uid}">|</li>
            ATagParams = class="active"
            linkWrap = |
        }

        IFSUB < .NO
        IFSUB = 1
        IFSUB.wrapItemAndSub = <li class="first {register:new} {register:firstnew} page-{field:uid}">|</li> |*| <li class="{register:new} page-{field:uid}">|</li> |*| <li class="last {register:new} {register:lastnew} page-{field:uid}">|</li>

        ACTIFSUB < .IFSUB
        ACTIFSUB.wrapItemAndSub = <li class="first {register:new} {register:firstnew} page-{field:uid} active">|</li> |*| <li class="{register:new} page-{field:uid} active">|</li> |*| <li class="last {register:new} {register:lastnew} page-{field:uid} active">|</li>
        ACTIFSUB.ATagParams = class="active"
        ACTIFSUB = 1
    }

    2 < .1
    2 {
        wrap = <ul>|</ul>
        NO = 1
        NO {
            wrapItemAndSub = <li class="first {register:new} {register:firstnew} page-{field:uid}">|</li> |*| <li class="{register:new} page-{field:uid}">|</li> |*| <li class="last {register:new} {register:lastnew} page-{field:uid}">|</li>
        }
        ACT < .NO
        ACT = 1
        ACT.ATagParams = class="active"
        ACT.wrapItemAndSub = <li class="active first {register:new} {register:firstnew} page-{field:uid}">|</li> |*| <li class="active {register:new} page-{field:uid}">|</li> |*| <li class="active {register:new} {register:lastnew} page-{field:uid}">|</li>
        ACTIFSUB < .ACT
        ACTIFSUB = 1
    }
    3 < .1
    3 {
        NO = 1
        NO {
            wrapItemAndSub = <li class="first {register:new} {register:firstnew} page-{field:uid}">|</li> |*| <li class="{register:new} page-{field:uid}">|</li> |*| <li class="last {register:new} {register:lastnew} page-{field:uid}">|</li>
        }
        ACT < .NO
        ACT = 1
        ACT.ATagParams = class="active"
        ACT.wrapItemAndSub = <li class="active first {register:new} {register:firstnew} page-{field:uid}">|</li> |*| <li class="active {register:new} page-{field:uid}">|</li> |*| <li<li class="active last {register:new} {register:lastnew} page-{field:uid}">|</li>
        ACTIFSUB < .ACT
        ACTIFSUB = 1
    }
    4 < .1
    4 {
        NO = 1
        NO {
            wrapItemAndSub = <li class="first {register:firstnew} page-{field:uid}">|</li> |*| <li class="{register:new} page-{field:uid}">|</li> |*| <li class="last {register:lastnew} page-{field:uid}">|</li>
        }
        ACT < .NO
        ACT = 1
        ACT.ATagParams = class="active"
        ACT.wrapItemAndSub = <li class="active first {register:firstnew} page-{field:uid}">|</li> |*| <li class="active {register:new} page-{field:uid}">|</li> |*| <li class="active last {register:lastnew} page-{field:uid}">|</li>
        ACTIFSUB < .ACT
        ACTIFSUB = 1
    }

}
lib.mainMenu = COA
lib.mainMenu {
    wrap =  <ul id="nav">|</ul>
	10 < lib.basemenu
	10 {
		1.wrap >
		1.expAll = 1
		2.expAll = 1
        2.wrapItemAndSub = <ul style="display: none;">|</ul>
	}
}

lib.breadcrumb = HMENU
lib.breadcrumb {
    special = rootline
    special.range = 1|-1

    1 = TMENU
    1 {
        NO = 1
        NO.allWrap = <li>|</li> &nbsp;>&nbsp;
        CUR = 1
        CUR.allWrap = <li>|</li>
        CUR.doNotLinkIt = 1
    }
}

lib.sideMenu = COA
lib.sideMenu {
    wrap =  <div id="cp-slide-menu" class="cp_side-navigation"><ul class="nav navbar-nav">|</ul></div>
    10 = TEXT
    10.value = <li id="close"><a href="#"><span class="icon-cancel micon"></span></a></li>

    20 < lib.basemenu
    20 {
        1.wrap >
        1.expAll = 1
        2.expAll = 1
        2.wrapItemAndSub = <ul class="dropdown-menu">|</ul>
    }
}

lib.footerMenu = HMENU
lib.footerMenu {    
    special = directory
    special.value = {$lib.footerMenu.directory}
    1 = TMENU
    1.wrap = <ul>|</ul>
    1 {
      noBlur = 1
      NO = 1
      NO {
        stdWrap.split {
          token = &
          cObjNum = 1
          1.current = 1
          1.wrap = |<br />
        }
        allWrap = <li>|</li>  
      }
    }
}
lib.language = HMENU
lib.language {
    special = language
    special.value = 0,1
    special.normalWhenNoLanguage = 0

    1 = GMENU
    1.wrap = <ul class="cp-lang-nav">|</ul>
    1.NO = 1
    1.NO {
        format = png
        // size of flag grafics
        XY = 18,12
        transparentColor = #00FFFF
        backColor = #00FFFF
        10 = IMAGE
        10.file = typo3conf/ext/tool_provider/Resources/Public/images/rs.png || typo3conf/ext/tool_provider/Resources/Public/images/gb.png
        #10.offset = 10,0
        10.file.width = 18
        10.file.height = 12

        wrap = <li class="lang-nav">|</li>
        doNotLinkIt = 1
        stdWrap {
            typolink {
                parameter.data = page:uid
                additionalParams = &L=0 || &L=1
                addQueryString = 1
                addQueryString.exclude = L,id,cHash,no_cache
                addQueryString.method = GET
                useCacheHash = 1
                no_cache = 0
            }
        }
    }


    1.ACT < .1.NO
    1.ACT.wrap = <li class="lang-nav active">|</li>

    # USERDEF1 <=> non-active languages
    1.USERDEF1 < .1.NO
    1.USERDEF1 = 1
    1.USERDEF1 {
        doNotLinkIt = 1
        wrapItemAndSub = <li class="na">|</li>
        stdWrap.typolink >
    }

	# USERDEF2 <=> active languages
    1.USERDEF2 < .1.ACT
    1.USERDEF2 = 1
    1.USERDEF2 {
        noLink = 1
    }
}

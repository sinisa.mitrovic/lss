page = PAGE
page.typeNum = 0
page {
    meta {
        viewport = width=device-width, initial-scale=1, maximum-scale=1
    }

    10 < lib.pageContent

    shortcutIcon =  EXT:tool_provider/Resources/Public/images/favicon.ico
}

page.900973 = TEXT
page.900973.value (
    <script type="text/javascript">

var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-47685859-1']);
_gaq.push(['_gat._anonymizeIp']);
_gaq.push(['_trackPageview']);

(function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

</script>
)




<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

// include TSConfig (backend configuration)
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/PageTSConfig/main.ts">');

